function updateForm(){
    var elem = document.getElementById('productType');
    var selected = elem.options[elem.selectedIndex].text;

    // Check which product type was selected and display appropriate box
    if (selected === 'Book') {
        book.style.display = 'block';
        furniture.style.display = 'none';
        disc.style.display = 'none';
        resetFields();

    }
    else if (selected === 'Furniture'){
        book.style.display = 'none';
        furniture.style.display = 'block';
        disc.style.display = 'none';
        resetFields();
    }
    else if (selected === 'DVD') {
        book.style.display = 'none';
        furniture.style.display = 'none';
        disc.style.display = 'block';
        resetFields();
    }
}

function resetFields(){
    document.getElementById('discSize').value = '';
    document.getElementById('bookWeight').value = '';
    document.getElementById('furnitureHeight').value = '';
    document.getElementById('furnitureWidth').value = '';
    document.getElementById('furnitureLength').value = '';
}