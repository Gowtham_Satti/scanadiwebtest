<?php include '../app/Views/layout/main_layout.php' ?>

    <div class="col-6">
        <h1>Product Add</h1>
    </div>
    <div class="col-6 d-flex" style="padding-left: 15%;">
        <div class="mx-2">
            <button type="submit" form="createProduct" class="btn btn-primary" name="save">Save</button>
        </div>
        <div>
            <a href="./productList" class="btn btn-primary">Cancel</a>
        </div>
    </div>
    </div>

    <hr class="mt-2 mb-3 col-10"/>

    <div class="row">
        <form id="createProduct" method="post" action="./create">
            <div class="text-danger">
                <?php echo $data['errorMsg']; ?>
            </div>
            <div class="form-group row">
                <label for="productSKU" class="col-sm-2 col-form-label">SKU</label>
                <div class="col-sm-5 mb-3">
                    <input type="number" class="form-control" id="productSKU" name="productSKU">
                </div>
            </div>
            <div class="form-group row">
                <label for="productName" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-5 mb-3">
                    <input type="text" class="form-control" id="productName" name="productName">
                </div>
            </div>
            <div class="form-group row">
                <label for="productPrice" class="col-sm-2 col-form-label">Price($)</label>
                <div class="col-sm-5 mb-3">
                    <input type="number" step=".01" class="form-control" id="productPrice" name="productPrice">
                </div>
            </div>
            <div class="form-group row">
                <label for="productType" class="col-sm-2 col-form-label">Type Switcher</label>
                <div class="col-sm-5 mb-3">
                    <select class="form-control" id="productType" onchange="updateForm();" name="productType">
                        <option value="DVD">DVD</option>
                        <option value="Book">Book</option>
                        <option value="Furniture">Furniture</option>
                    </select>
                </div>
            </div>
            <div class="form-group row" id="disc" style="display: block;">
                <label for="discSize" class="col-sm-2 col-form-label">Size(MB)</label>
                <div class="col-sm-5 mb-3">
                    <input type="number" id="discSize" class="form-control" aria-describedby="discHelpBlock"
                           name="productQTY[]">
                </div>
                <small id="discHelpBlock" class="form-text text-muted">
                    Please, provide size in MB.
                </small>
            </div>
            <div class="form-group row" id="book" style="display: none;">
                <label for="bookWeight" class="col-sm-2 col-form-label">Weight(KG)</label>
                <div class="col-sm-5 mb-3">
                    <input type="number" step=".01" id="bookWeight" class="form-control"
                           aria-describedby="bookHelpBlock" name="productQTY[]">
                </div>
                <small id="bookHelpBlock" class="form-text text-muted">
                    Please, provide weight in KG.
                </small>
            </div>
            <div class="form-group row" id="furniture" style="display: none;">
                <label for="furnitureHeight" class="col-sm-2 col-form-label">Height(CM)</label>
                <div class="col-sm-5 mb-3">
                    <input type="number" step=".01" id="furnitureHeight" class="form-control"
                           aria-describedby="bookHelpBlock" name="productQTY[]">
                </div>
                <label for="furnitureWidth" class="col-sm-2 col-form-label">Width(CM)</label>
                <div class="col-sm-5 mb-3">
                    <input type="number" step=".01" id="furnitureWidth" class="form-control"
                           aria-describedby="bookHelpBlock" name="productQTY[]">
                </div>
                <label for="furnitureLength" class="col-sm-2 col-form-label">Length(CM)</label>
                <div class="col-sm-5 mb-3">
                    <input type="number" step=".01" id="furnitureLength" class="form-control"
                           aria-describedby="bookHelpBlock" name="productQTY[]">
                </div>
                <small id="bookHelpBlock" class="form-text text-muted">
                    Please, provide dimensions in HxWxL format.
                </small>
            </div>
        </form>
    </div>
    </div>

<?php include '../app/Views/layout/footer.php' ?>