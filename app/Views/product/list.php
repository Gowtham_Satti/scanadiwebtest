<?php include '../app/Views/layout/main_layout.php' ?>

    <div class="col-6">
        <h1>Product List</h1>
    </div>
    <div class="col-6 d-flex" style="padding-left: 15%;">
        <div class="mx-2">
            <a href="./add" class="btn btn-primary mr-4">ADD</a>
        </div>
        <div>
            <button type="submit" form="list" class="btn btn-primary" name="remove">MASS DELETE</button>
        </div>
    </div>
    </div>

    <hr class="mt-2 mb-3 col-10"/>

    <div class="row">
        <form id="list" method="post" action="./delete">
            <?php
                echo $data['products'];
            ?>
        </form>
    </div>

<?php include '../app/Views/layout/footer.php' ?>