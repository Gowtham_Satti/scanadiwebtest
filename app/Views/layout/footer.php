<div class="navbar fixed-bottom">
    <footer class="col-12 bg-light text-center text-lg-start">
        <div class="text-center p-3">
            <h5 class="text-uppercase">Scandiweb Test Assignment</h5>
        </div>
    </footer>
</div>
<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
<script src="../js/script.js"></script>
</body>
</html>