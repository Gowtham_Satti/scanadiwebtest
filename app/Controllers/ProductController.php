<?php

class ProductController extends Controller
{
    private $connection;
    private $messages;
    private $objectArray;
    protected $table_name = 'products';

    public function __construct()
    {
        $db = $this->model('Database');
        $this->connection = $db->getConnection();
        $this->messages = '';
        $this->objectArray = array(
            "DVD" => $this->model('Dvd'),
            "Book" => $this->model('Book'),
            "Furniture" => $this->model('Furniture')
        );
    }

    public function Index(){
        header('location: ./Product/productList');
    }

    public function add(){
        $this->view('product/add', ['title' => 'Add Product' ,'errorMsg'=> $this->getMessage()]);
    }

    public function create(){
        if (isset($_POST['save'])){

            if (!empty($_POST['productSKU']) &&
                !empty($_POST['productName']) &&
                !empty($_POST['productPrice']) &&
                !empty($_POST['productType']) &&(
                    !empty($_POST['productQTY'][0]) ||
                    !empty($_POST['productQTY'][1]) ||
                    (!empty($_POST['productQTY'][2]) &&
                        !empty($_POST['productQTY'][3]) &&
                        !empty($_POST['productQTY'][4])))) {
                $productQTY = array_filter($_POST['productQTY']);
                $productQTY = array_values($productQTY);
                if (sizeof($productQTY) > 1) {
                    $qty = $productQTY[0] . ' x ' . $productQTY[1] . ' x ' . $productQTY[2];
                } else {
                    $qty = $productQTY[0];
                }

                $product = $this->objectArray[strip_tags($_POST['productType'])];
                $sku = strip_tags($_POST['productSKU']);
                $price = doubleval(strip_tags($_POST['productPrice']));
                $name = strip_tags($_POST['productName']);
                $type = strip_tags($_POST['productType']);

                if ($this->validate($price, $type)) {
                    $product->setSKU($sku);
                    $product->setName($name);
                    $product->setPrice($price);
                    $product->setType($type);
                    $product->setQTY($qty);
                    $this->insert($product);
                }else {
                    $this->messages = 'Please, provide the data of indicated type';
                    header('location: ./add');
                }

            }else{
                $this->messages = 'Please, submit required data';
                header('location: ./add');
            }
        }
    }

    public function productList(){
        $this->view('product/list', ['title' => 'Products List', 'products' => $this->showAllProducts()]);
    }

    public function delete(){
        $sku_list = $this->getSKUList();
        $productArray = $_POST['marked'];

        for ($i=0; $i < sizeof($productArray); $i++){
            $index = $productArray[$i];

            $query = "DELETE FROM " . $this->table_name . " WHERE SKU=:sku";

            $stmt = $this->connection->prepare($query);

            $stmt->bindParam(':sku', $sku_list[$index]);

            if ($stmt->execute()){
                echo '<script type="text/javascript">
                        window.location.href = "./productList";
                    </script>';
            }else{
                echo '<script type="text/javascript">
                        window.location.href = "./productList";
                    </script>';
            }

        }
    }

    private function validate($price, $type){

        $fprice = filter_var($price, FILTER_VALIDATE_FLOAT);
        if ($type == 'DVD' || $type == 'Book' || $type == 'Furniture' ){
            $ftype = true;
        }else{
            $ftype = false;
        }

        if (!($fprice && $ftype)){
            return false;
        }else{
            return true;
        }
    }

    private function insert($product){
        $sku = $product->getSKU();
        $name = $product->getName();
        $price = $product->getPrice();
        $type = $product->getType();
        $qty = $product->getQTY();

        if (!$this->skuExists($sku)) {
            $query = "INSERT INTO " . $this->table_name . " SET SKU=:sku, name=:name, price=:price, type=:type, qty=:qty;";

            $stmt = $this->connection->prepare($query);

            $stmt->bindParam(':sku', $sku);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':price', $price);
            $stmt->bindParam(':type', $type);
            $stmt->bindParam(':qty', $qty);

            if ($stmt->execute()) {
                header('location: ./productList');
            }

        }else{
            $this->messages= 'SKU must be unique';
            $this->add();
        }
    }

    private function skuExists($sku)
    {
        $exists = false;
        $skuList = $this->getSKUList();
        if ($skuList != null) {
            foreach ($skuList as $skuNum) {
                if ($skuNum == $sku) {
                    $exists = true;
                    break;
                }
            }
        }
        return $exists;
    }

    private function getMessage(){
        return $this->messages;
    }

    private function getAllProducts(){

        $query = "SELECT * FROM products ORDER BY SKU";

        $stmt = $this->connection->prepare($query);

        $stmt->execute();
        if ($stmt->rowCount() > 0){
            $result = $stmt->fetchAll();
            return $result;
        }else{
            return null;
        }
    }

    private function showAllProducts(){
        $products = $this->getAllProducts();
        $output = '';
        if ($products != null) {
            $items = 4;
            $checKID = 0;

            foreach ($products as $pro) {
                $product = $this->objectArray[$pro['type']];

                $product->setSKU($pro['SKU']);
                $product->setName($pro['name']);
                $product->setPrice($pro['price']);
                $product->setType($pro['type']);
                $product->setQTY($pro['qty']);

                if ($items == 4) {
                    $output .= '<div class="row">';
                }
                $items--;
                $output .= '<div class="col-3 d-flex border">';
                $output .= '<div class="float-left">' .
                    '<input type="checkbox" id=' . $checKID . ' name="marked[]" value=' . $checKID . '></div>';

                $checKID++;

                $output .= $this->generateHTML($product);

                if ($items == 0) {
                    $output .= '</div>';
                    $items = 4;
                }

            }

            if ($items != 0) {
                $output .= '</div>';
            }
        }
        else{
            $output .= "There are no products in the Database";
        }
        return $output;
    }

    private function getSKUList(){
        $products = $this->getAllProducts();
        if ($products != null) {
            $sku_list = array();

            foreach ($products as $pro) {
                $sku_list[] = $pro['SKU'];
            }
            return $sku_list;
        }else{
            return null;
        }
    }

    private function generateHTML($product){
        $output = '';
        $output .='<p style="padding-left:2%;">'.
            $product->getSKU().'<br>'.
            $product->getName().'<br>'.
            $product->getPrice().'<br>';

        switch ($product->getType()){
            case 'Dvd':
                $output .= 'Size: '.$product->getQTY(). 'MB';
                break;
            case 'Book':
                $output .= 'Weight: '.$product->getQTY(). 'KG';
                break;
            case 'Furniture':
                $output .= 'Dimensions: '.$product->getQTY();
                break;
            default:
                $output .= 'Size: '.$product->getQTY(). 'MB';
        }

        $output .= '<br></p>'.
            '</div>';
        return $output;
    }
}