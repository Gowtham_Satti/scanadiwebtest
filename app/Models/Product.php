<?php

class Product{

    protected $SKU;
    protected $name;
    protected $price;
    protected $type;
    
    public function getSKU()
    {
        return $this->SKU;
    }
    
    public function setSKU($SKU)
    {
        $this->SKU = $SKU;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function getPrice()
    {
        return $this->price;
    }
    
    public function setPrice($price)
    {
        $this->price = $price;
    }
    
    public function getType()
    {
        return $this->type;
    }
    
    public function setType($type)
    {
        $this->type = $type;
    }
}
?>