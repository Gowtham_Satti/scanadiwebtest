<?php

class Database
{
    private $HOST = '<HOSTNAME>';
    private $DB = '<DB>';
    private $USER = '<USERNAME>';
    private $PASS = '<PASSWORD>';
    public $connection;

    public function getConnection(){
        $this->connection = null;
        try {
            $this->connection = new PDO("mysql:host=" . $this->HOST . ";dbname=" . $this->DB,
                $this->USER,
                $this->PASS);
        }catch(PDOException $pe){
            echo "Error: " . $pe->getMessage();
        }
        return $this->connection;
    }    
}
?>